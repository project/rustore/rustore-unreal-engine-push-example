package ru.rustore.unreal.pushclient;

public enum LogMessageType {
    DEBUG,
    ERROR,
    INFO,
    VERBOSE,
    WARN
}
