package ru.rustore.unreal.pushclient

enum class UnrealClientIdType {
    GAID,
    OAID,
}
