## История изменений

### Release 6.8.0
- Версия SDK push 6.8.0.

### Release 6.7.0
- Версия SDK push 6.7.0.

### Release 6.6.1
- Версия SDK push 6.6.1.

### Release 6.5.1
- Версия SDK push 6.5.1.

### Release 6.4.0
- Версия SDK push 6.4.0.

### Release 6.3.0
- Версия SDK push 6.3.0.
- Класс `ru.rustore.unitysdk.pushclient.RuStoreUnityMessagingService` помечен как устаревший, вместо него используется класс `ru.rustore.unreal.pushclient.RuStoreUnrealMessagingService`.
- Исправлена проблема с логированием уровня `Warn` в классе `URuStoreLogListener`, приводившая к остановке приложения при получении записи лога.

### Release 6.2.1
- Версия SDK push 6.2.1.
- Добавлено поле `clickActionType` в структуру `FURuStoreNotification`.

### Release 6.1.0
- Версия SDK push 6.1.0.

### Release 1.4
- Версия SDK push 1.+.

### Release 1.2.0
- Версия SDK push 1.2.0.
- Добавлен функционал [сегментов](https://www.rustore.ru/help/sdk/push-notifications/using-segments).

### Release 1.0
- Версия SDK push 1.0.0.
